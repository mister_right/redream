const app = require('express')()
var cors = require('cors')
const bodyParser = require('body-parser')

const { Pool } = require('pg')

const connectionString = process.env.REDREAM_PSQL_CONN_STRING
const pool = new Pool({ connectionString })

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
const port = 3000

app.use(cors({ origin: '*' }))

app.post('/message', async (req, res) => {
    res.header('')
    console.log(req.body)
    if (!req.body.email || !req.body.subject || !req.body.message) {
        res.status(400).send('Bad Request')
        return
    }
    await pool.query('insert into messages(email, subject, message) values ($1, $2, $3)', [req.body.email, req.body.subject, req.body.message])
    res.send(req.body)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
