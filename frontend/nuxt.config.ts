// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: [
    '@/assets/main.css',
  ],
  modules: [
    '@nuxtjs/tailwindcss'
  ],
  runtimeConfig: {
    public: {
      apiBase: 'https://api.redream.pl',
    }
  },
  //nitro: {
  //  prerender: {
  //   routes: ['/', '/team', '/kontakt', '/realizacje', '/oferty', '/realizacje/19', '/realizacje/18', '/realizacje/17', '/oferty/37', '/oferty/38']
  //  }
  //}
})
