const express = require('express')
const { Pool } = require('pg')
const cors = require('cors')

const connectionString = process.env.REDREAM_PSQL_CONN_STRING
const pool = new Pool({ connectionString })

const app = express()
app.use(cors({ origin: '*' }))
const port = 3000

app.get('/offers', async (req, res) => {
    const offers = await pool.query('select a.offer_id, a.title_0, a.description_0, a.items_dict, b.image_name from offers a join images b on a.img_0 = b.image_id')
    const rows = []
    for (let i of offers.rows) {
        i['img_0'] = `https://img.redream.pl/${i.image_name}`
        rows.push(i)
    }
    res.send(rows)
})

app.get('/offers/:id', async (req, res) => {
    const offer = await pool.query('select a.title_1, a.title_2, a.description_1, a.items_list, b.image_name from offers a join images b on a.img_1 = b.image_id where a.offer_id = $1::int', [req.params.id])
    const data = offer.rows[0]
    data['img_1'] = `https://img.redream.pl/${data.image_name}`
    res.send(data)
})

app.get('/realizations', async (req, res) => {
    const realizations = await pool.query('select a.realization_id, a.line_0, a.line_1, b.image_name from realizations a join images b on a.main_image_id = b.image_id')
    for (let i of realizations.rows) {
        i['img_0'] = `https://img.redream.pl/${i.image_name}`
    }
    res.send(realizations.rows)
})

app.get('/realizations/:id', async (req, res) => {
    const images = await pool.query('select b.image_name from realization_images a inner join images b on a.image_id = b.image_id and a.realization_id = $1::int', [req.params.id])
    const urls = []
    for (let i of images.rows) {
        urls.push(`https://img.redream.pl/${i.image_name}`)
    }
    res.send(urls)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
