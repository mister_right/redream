import os
import uuid

from fastapi import APIRouter, Depends, File, HTTPException, UploadFile, status
from fastapi.responses import FileResponse, JSONResponse

from libs.common import decode_image, encode_image, verify_token
from libs.psql import db

router = APIRouter()


@router.post('/', dependencies=[Depends(verify_token)])
async def image_upload(image: UploadFile = File(...)):
    filename = str(uuid.uuid4()) + '-' + image.filename
    filepath = f'/static/images/{filename}'
    query = 'insert into images (image_name, image_path, uploaded_by) values (:name, :path, :by)'
    await db.execute(query, {'name': filename, 'path': filepath, 'by': 0})
    content = await image.read()
    with open(filepath, 'wb') as f:
        f.write(content)
    return image.filename


@router.get('/count', dependencies=[Depends(verify_token)])
async def get_number_of_images():
    return await db.fetch_one('select count(*) from images')


@router.get('/', dependencies=[Depends(verify_token)])
async def get_images(page: int, page_size: int, desc: bool, get_count=False):
    order = 'desc' if desc is True else 'asc'
    limit, offset = page_size, (page - 1) * page_size
    images = await db.fetch_all(f'select image_path from images order by uploaded_at {order} limit {limit} offset {offset}')
    return [encode_image(i._mapping['image_path']) for i in images]


@router.get('/{encoded_path}')
async def get_image(encoded_path: str):
    image_path = decode_image(encoded_path)
    print(image_path)

    if os.path.isfile(image_path) is False:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return FileResponse(image_path)


@router.delete('/{encoded_path}', dependencies=[Depends(verify_token)])
async def destroy_image(encoded_path: str):
    image_path = decode_image(encoded_path)
    if os.path.isfile(image_path) is False:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    await db.execute('delete from images where image_path = :image_path', {'image_path': image_path})
    return JSONResponse(status_code=status.HTTP_200_OK, content={'message': 'image DESTROYED'})
