import json

from fastapi import APIRouter, Depends, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel

from libs.common import encode_image, decode_image, verify_token
from libs.psql import db

router = APIRouter()


class Offer(BaseModel):
    title_0: str
    title_1: str
    title_2: str
    description_0: str
    description_1: str
    img_0: str
    img_1: str             
    items_dict: dict
    items_list: dict


@router.post('/', dependencies=[Depends(verify_token)])
async def create_offer(offer: Offer):
    query = 'select image_id from images where image_path = :path'
    img_0 = await db.fetch_one(query, {'path': decode_image(offer.img_0)})
    img_1 = await db.fetch_one(query, {'path': decode_image(offer.img_1)})
    img_0 = img_0._mapping['image_id']
    img_1 = img_1._mapping['image_id']

    query = '''insert into offers (title_0, title_1, title_2, description_0, description_1, img_0, img_1, items_dict, items_list) 
        values (:t_0, :t_1, :t_2, :d_0, :d_1, :i_0, :i_1, :id, :il)'''

    values = {
        't_0': offer.title_0,
        't_1': offer.title_1,
        't_2': offer.title_2,
        'd_0': offer.description_0,
        'd_1': offer.description_1,
        'i_0': img_0,
        'i_1': img_1,
        'id': json.dumps(offer.items_dict),
        'il': json.dumps(offer.items_list)
    }
    await db.execute(query, values)
    return JSONResponse(status_code=status.HTTP_201_CREATED, content={'message': 'offer created'})


@router.delete('/{offer_id}', dependencies=[Depends(verify_token)])
async def delete_offer(offer_id: int):
    await db.execute('delete from offers where offer_id = :id', {'id': offer_id})
    return JSONResponse(status_code=status.HTTP_200_OK, content={'message': 'offer deleted'})


@router.put('/{offer_id}', dependencies=[Depends(verify_token)])
async def update_offer(offer_id: int, offer: Offer):
    query = 'select image_id from images where image_path = :path'
    img_0 = await db.fetch_one(query, {'path': decode_image(offer.img_0)})
    img_1 = await db.fetch_one(query, {'path': decode_image(offer.img_1)})
    img_0 = img_0._mapping['image_id']
    img_1 = img_1._mapping['image_id']

    query = '''update offers set title_0 = :t_0, title_1 = :t_1, title_2 = :t_2, description_0 = :d_0, description_1 = :d_1,
        img_0 = :i_0, img_1 = :i_1, items_dict = :items_dict, items_list = :il where offer_id = :id'''
    values = {
        't_0': offer.title_0,
        't_1': offer.title_1,
        't_2': offer.title_2,
        'd_0': offer.description_0,
        'd_1': offer.description_1,
        'i_0': img_0,
        'i_1': img_1,
        'items_dict': json.dumps(offer.items_dict),
        'il': json.dumps(offer.items_list),
        'id': offer_id
    }
    await db.execute(query, values)
    return JSONResponse(status_code=status.HTTP_201_CREATED, content={'message': 'offer updated'})


@router.get('/', dependencies=[Depends(verify_token)])
async def get_offers(limit: int = 20, offset: int = 0):
    query = 'select offer_id, title_0, img_0 from offers limit :limit offset :offset'
    offers = await db.fetch_all(query, {'limit': limit, 'offset': offset})

    response = []
    for offer in offers:
        offer = dict(offer._mapping)
        image = await db.fetch_one('select image_path from images where image_id = :id', {'id': offer['img_0']})
        offer['img_0'] = encode_image(image._mapping['image_path'])
        response.append(offer)
    return response


@router.get('/{offer_id}')
async def get_offer(offer_id: int):
    offer = await db.fetch_one('select * from offers where offer_id = :id', {'id': offer_id})
    offer = dict(offer._mapping)

    query = 'select image_path from images where image_id = :id'
    img_0 = await db.fetch_one(query, {'id': offer['img_0']})
    img_1 = await db.fetch_one(query, {'id': offer['img_1']})

    offer['img_0'] = encode_image(img_0._mapping['image_path'])
    offer['img_1'] = encode_image(img_1._mapping['image_path'])
    offer['items_list'] = json.loads(offer['items_list'])
    offer['items_dict'] = json.loads(offer['items_dict'])
    return offer
