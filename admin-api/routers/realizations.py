from fastapi import APIRouter, Depends, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel

from libs.common import decode_image, encode_image, verify_token
from libs.psql import db

router = APIRouter()


class Realization(BaseModel):
    line_0: str
    line_1: str
    images: list


@router.post('/')
async def create_realization(realization: Realization):
    image_paths = tuple([decode_image(i) for i in realization.images])
    query = f'select image_path, image_id from images where image_path in {image_paths}'
    images = await db.fetch_all(query)
    images_name_id = {i._mapping['image_path']: i._mapping['image_id'] for i in images}

    query = 'insert into realizations(line_0, line_1, main_image_id) values(:l_0, :l_1, :image_id) returning realization_id' 
    values = {'l_0': realization.line_0, 'l_1': realization.line_1, 'image_id': images_name_id[image_paths[0]]}
    new_id = await db.fetch_one(query, values)

    new_id = tuple(new_id.values())[0]
    query = 'insert into realization_images (realization_id, image_id) values (:rid, :iid)'
    await db.execute_many(query, [{'rid': new_id, 'iid': images_name_id[i]} for i in image_paths])
    return JSONResponse(status_code=status.HTTP_200_OK, content={'message': f'realization {new_id} added'})


@router.get('/')
async def get_realizaiotns(limit: int = 10, offset: int = 0, desc: bool = False):
    order = 'desc' if desc is True else 'asc'
    realizations = await db.fetch_all(f'select * from realizations order by last_update {order} limit {limit} offset {offset} ')

    to_return = []
    query = 'select a.image_id, b.image_path from realization_images a join images b on a.realization_id = :rid and a.image_id = b.image_id'
    for realization in realizations:
        images = await db.fetch_all(query, {'rid': realization.realization_id})
        images_dict = {i._mapping['image_id']: encode_image(i._mapping['image_path']) for i in images}
        r = dict(realization._mapping)
        r['images'] = list(images_dict.values())
        r['main_image_name'] = images_dict[r['main_image_id']]
        to_return.append(r)
    return {i['realization_id']: i for i in to_return}


@router.get('/count')
async def get_realizaiotns_count():
    return await db.execute('select count(*) from realizations')


@router.delete('/{realization_id}')
async def delete_realozation(realization_id: int):
    await db.execute('delete from realization_images where realization_id = :id', {'id': realization_id})
    await db.execute('delete from realizations where realization_id = :id', {'id': realization_id})
    return JSONResponse(status_code=status.HTTP_200_OK, content={'message': 'realization deleted'})
