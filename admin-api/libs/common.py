import base64
import bcrypt
import datetime
import jwt
import os

from fastapi import Header, HTTPException

BASE_URL = os.environ.get('BASE_URL', 'http://localhost:8000')
key = os.environ.get('JWT_KEY', '123')


def check_password(plain_text_password, hashed_password):
    return bcrypt.checkpw(plain_text_password.encode(), hashed_password)


def get_hashed_password(plain_text_password):
    return bcrypt.hashpw(plain_text_password.encode(), bcrypt.gensalt())


def generate_jwt_token(user_id):
    return jwt.encode({'user_id': user_id}, key, algorithm="HS256")


async def get_user_id(token: str | None = Header(default=None)):
    user_id = None
    try:
        payload = jwt.decode(token, key, ['HS256'])
        user_id = payload['user_id']
    except jwt.exceptions.DecodeError:
        pass

    return user_id


async def verify_token(token: str = Header()):
    try:
        payload = jwt.decode(token, key, ['HS256'])
    except jwt.exceptions.DecodeError:
        raise HTTPException(status_code=403, detail='Invalid token.')
    return payload['user_id']


def encode_image(path):
    return BASE_URL + '/images/' + str(base64.urlsafe_b64encode(path.encode()).decode())


def decode_image(path):
    return str(base64.urlsafe_b64decode(path.split('/')[-1].encode()).decode())
