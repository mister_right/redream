from fastapi import HTTPException, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from libs.psql import db
from routers import images, offers, realizations, users

app = FastAPI()


@app.on_event("startup")
async def startup():
    await db.connect()


@app.on_event("shutdown")
async def shutdown():
    await db.disconnect()


app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.include_router(images.router, prefix='/images')
app.include_router(offers.router, prefix='/offers')
app.include_router(realizations.router, prefix='/realizations')
app.include_router(users.router, prefix='/users')
